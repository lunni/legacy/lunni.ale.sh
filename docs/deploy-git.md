You can deploy your own apps on Lunni, too. Let's create a simple FastAPI app as an example.

```py
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}
```


## Basic Docker setup

First, let's do a basic Docker setup – a Dockerfile:

```dockerfile
# Feel free to use your favourite base image here
FROM python:3
WORKDIR /app
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY app.py /app/app.py

CMD ["uvicorn", "app.main:app", "--host", "0"]
```

A `requirements.txt` file:

```
fastapi
```

And a super simple Compose file (for development):

```yaml
version: "3"
services:
  web:
    build: .
    ports:
      - "3000:3000"
```

Now you can just run `docker-compose up` and see it's working!


## Building Docker images

Docker Swarm can only run pre-built images, so in order to deploy things with Lunni, we'll need to build an image ourselves. If you make an open source app, that's easy:

```sh
docker build -t <user>/<app> .
docker push <user>/<app>
```

Note that this will push it to Docker Hub, which is publically accessible. Follow [the previous guide](./deploy.md) and you'll be up and running in no time!


## Using GitLab Container Registry

In commercial development however you'll probably want to restrict access to the image most of the time. There are various storage solutions (some of which you can run on Lunni too :) but for super easy start you can use GitLab's [Container Registry](https://gitlab.com/help/user/packages/container_registry/index). Just set up a private project on GitLab.com (if you don't have one for this project already) and run this:

```sh
docker login registry.gitlab.com
# You might have to use a personal access token instead of password if you have 2FA enabled

docker build -t registry.gitlab.com/<user>/<repo> .
docker push registry.gitlab.com/<user>/<repo>
```

Now we need to authenticate on server so that we can fetch the image. Username / password will work but a better idea would be to [create a personal access token](https://gitlab.com/help/user/profile/personal_access_tokens.md):

1. In the upper-right corner, click your avatar and select **Settings**.
2. On the **User Settings** menu, select **Access Tokens**.
3. Choose a name and optional expiry date for the token.
4. Choose the `read_registry` scope.
5. Click the **Create personal access token** button.

Back in Portainer, go to **Registries** → **Add registry** and fill in:

- **Name:** `GitLab.com`
- **Registry URL:** `registry.gitlab.com`
- **Authentication:** on
- **Username:** your GitLab.com username
- **Password:** your personal access token

You're good to go! Just use `registry.gitlab.com/<user>/<repo>` as `image` in the Compose file and follow [the previous guide](./deploy.md).

<!-- TODO elaborate here instead -->


## Building images on GitLab CI

GitLab also offers a powerful CI system that can be used to build and deploy your app automatically. A default template would do:

```yaml
docker-build-master:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master
```

To turn this from CI to a CD, we'll need to add one more step. Open your stack in Portainer, select your app service and turn in **Service webhook**. You'll get a link that looks like this: `https://portainer.<your-hostname>/api/webhooks/<hook-secret>`. When you send a POST request to it, Lunni will pull the latest version of the image. You can use it in your CI config like this:

```yaml
docker-build-master:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
    - curl -X POST "https://portainer.<your-hostname>/api/webhooks/<hook-secret>"
  only:
    - master
```

Now when you push something to `master`, you'll get it running on your server automatically! Just don't forget to add some testing steps too ;-)
